const Reveal = require('reveal.js');

const highlight = require('highlight.js')

require('reveal.js/css/reveal.css');
require('reveal.js/css/theme/black.css');

require('highlight.js/styles/zenburn.css');

require('./styles.css');

window.stampit = require('stampit');

Reveal.initialize({
    controls: true,
    progress: true,
    history: true,
    center: true,
    // default/cube/page/concave/zoom/linear/fade/none 
    transition: 'concave',
});

highlight.initHighlightingOnLoad();